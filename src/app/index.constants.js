
(function() {
    'use strict';

    angular.module('scoresy')
        .constant ('apiEndPointsConstant', {
            appId: 'q4MDAEVe1I6BAOSeJhgSbAalVO7ADwS9nxKN2pQp',
            restApiKey: 'LmBCTwKAqdCJpZbYEhHq8CH2SS8ZlJROPaq2ahFU',
            
            getPlayers: 'https://api.parse.com/1/classes/_User',
            getTeams: 'https://api.parse.com/1/classes/Team',
            getCompleteContests: 'https://api.parse.com/1/functions/getCompleteContests',
            // getContests: 'https://api.parse.com/1/classes/Contest',
            // getContestFullData: 'https://api.parse.com/1/functions/getContestFullData',

            saveContest: 'https://api.parse.com/1/functions/saveContestFullData',
            addNewPlayer: 'https://api.parse.com/1/functions/addNewUser',
            login: 'https://api.parse.com/1/functions/login'
        });

})();
