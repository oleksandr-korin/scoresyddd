(function() {
    'use strict';

    angular
        .module('scoresy', ['ngAnimate', 'ui.router', 'ngMaterial', 'LocalStorageModule', 'ngCookies']);

})();
