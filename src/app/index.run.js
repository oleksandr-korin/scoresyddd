	(function() {
    'use strict';

    angular
        .module('scoresy')
        .run(runBlock);

    /** @ngInject */
    function runBlock($log, $rootScope, UserAppService, $state) {
    	$rootScope.$on('$stateChangeSuccess', function(e, toState, toParams, fromState, fromParams) {


    		if (!UserAppService.isLoggedIn() && (fromState.name !== 'login')) {
    			$state.go('login');
    		}

    	});
    }

})();
