
(function() {
    'use strict';

    angular
        .module('scoresy')
        .controller('ContestCtrl', ContestCtrl);

    /** @ngInject */
    function ContestCtrl($state, $scope, ContestAppService) {

        this.createContest = function() {
            ContestAppService.createNewContest($scope.contest);

            $state.go('chooseTeams');
        };
    }
})();
