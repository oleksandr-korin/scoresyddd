
(function() {
    'use strict';

    angular
        .module('scoresy')
        .controller('MatchInProgressCtrl', MatchInProgressCtrl);

    /** @ngInject */
    function MatchInProgressCtrl($state, $scope, $mdDialog, MatchAppService, $timeout) {
        var matchInProgress = MatchAppService.getMatchInProgress();

        $scope.team1 = matchInProgress.team_1;
        $scope.team2 = matchInProgress.team_2;

        $scope.scoreTheGoal = function(teamNumber) {
            var owner, assistant, activityData,
                teamScored = (teamNumber === 1) ? $scope.team1 : $scope.team2;
                teamScoredAutogoal = (teamNumber === 1) ? $scope.team2 : $scope.team1;

                $mdDialog.show({
                    controller: function($scope) {
                        $scope.players = teamScored.players;
                        $scope.playersAutogoal = teamScoredAutogoal.players;

                        $scope.toggleAutogoal = function() {
                        }

                        $scope.confirmActivity = function() {
                            $mdDialog.hide();
                        };
                        $scope.cancelActivity = function() {
                            $mdDialog.cancel();
                        };

                        $scope.$watch('activityData', function(val) {
                            activityData = val;
                        }, true);
                    },
                    templateUrl: 'app/components/contest/templates/modal/newActivityDialog.html',
                    parent: angular.element(document.body),
                }).then(function() {
                    var isFinalGoal;
                    isFinalGoal = MatchAppService.scoreTheGoal(matchInProgress, teamScored, activityData);

                    if (isFinalGoal) {
                        $state.go('matchesList');
                    }
                });
        };

        $scope.pauseMatch = function() {
            $mdDialog.show({
                controller: function($scope) {
                    $scope.confirmFinish = function() {
                        $mdDialog.hide();
                    };
                    $scope.cancelFinish = function() {
                        $mdDialog.cancel();
                    };
                },
                templateUrl: 'app/components/contest/templates/modal/pauseMatchDialog.html',
                parent: angular.element(document.body),
            }).then(function() {
                MatchAppService.finnishMatch();
                $state.go('matchesList');
            });
        };

        $scope.getTeam1Score = function() {
            return matchInProgress.getTeam1Score();
        };

        $scope.getTeam2Score = function() {
            return matchInProgress.getTeam2Score();
        };

    }
    
})();
