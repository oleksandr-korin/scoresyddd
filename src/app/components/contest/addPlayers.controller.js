
(function() {
    'use strict';

    angular
        .module('scoresy')
        .controller('AddPlayersCtrl', AddPlayersCtrl);

    /** @ngInject */
    function AddPlayersCtrl($state, $scope, RemoteDataService, ContestAppService, $rootScope, $mdDialog) {
        $scope.teamSelected;

        // init data
        _initPlayersList();
        _initTeamsList();
        
        $scope.addPlayersToTeam = function(team) {
            $scope.teamSelected = team;
            $scope.playersListShown = true;
        };

        $scope.completeSelection = function() {
            $scope.playersListShown = false;

            angular.forEach($scope.teamsChosen, function(team) {
                team.isExpanded = false;
            });
        };

        $scope.addPlayer = function(player) {
            ContestAppService.togglePlayerSelection($scope.teamSelected, player);
        };

        $scope.removePlayer = function(team, player) {
            ContestAppService.removePlayerFromTeam(team, player);

            if(team.getNumberOfAddedPlayers() < 1) {
                $scope.toggleCollapseForTeam(team);
            }
        };

        $scope.ifPlayerSelectedByTheTeam = function(player) {
            ContestAppService.ifPlayerSelectedByTheTeam($scope.teamSelected, player);
        };

        $scope.getSelectedStyle = function(player) {
            var style = {}, isSelected = false;
                style['background'] = $scope.teamSelected.color;
                style['font-weight'] = 'bold';
                style['border-bottom'] = '1px solid #ddd';

                isSelected = $scope.teamSelected.checkIfAdded(player);

                if (isSelected) {
                    return style;
                }
        };

        $scope.getStringForSelectedNumber = function(teamObj) {
            if (!teamObj) {
                return '0 игроков';
            };

            var numberSelected, string = ' игрок';
                numberSelected = teamObj.getNumberOfAddedPlayers();

                if (numberSelected  > 1 && numberSelected < 5) string += 'а';
                if (numberSelected  > 4 || numberSelected === 0) string += 'ов';

            return numberSelected + string;
        };

        $scope.toggleCollapseForTeam = function(team) {
            if (team.isExpanded === undefined) {
                team.isExpanded = true;
                return;
            }

            team.isExpanded = !team.isExpanded;
        };

        $scope.addNewPlayerDialog = function(ev) {
            $mdDialog.show({
                controller: AddPlayersCtrl,
                templateUrl: 'app/components/contest/templates/modal/addPlayerDialog.html',
                parent: angular.element(document.body),
                targetEvent: ev,
            }).then(function() {

                _initPlayersList();
            });
        };

        $scope.addNewPlayer = function(newPlayer) {
            ContestAppService.addNewPlayer(newPlayer)
                .then(function() {
                    $scope.closeDialog();
                }, function(error) {
                    $scope.addplayerError = error.data.error
                });
        };

        $scope.closeDialog = function() {
            $mdDialog.hide();
        };

        $scope.goToMatchesList = function() {
            $state.go('matchesList');
        };

        // // // // // // // 
        // private functions
        // // // // // // // 

        function _initTeamsList() {
            $scope.teamsChosen = ContestAppService.getTeamsOfContest();

            angular.forEach($scope.teamsChosen, function(team) {
                team.isExpanded = false;
            });
        }

        function _initPlayersList() {
            RemoteDataService.getDataListForClass('Player').then(function(response) {
                $scope.allPlayers = response.data.results;
            });
        }

        // // // // // // // // 
        // Events subscription
        // // // // // // // // 

        $rootScope.$on('applySearch', function(e, data) {
            $scope.search = data.string;
        });

    }
})();
