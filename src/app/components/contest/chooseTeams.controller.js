
(function() {
    'use strict';

    angular
        .module('scoresy')
        .controller('ChooseTeamsCtrl', ChooseTeamsCtrl);

    /** @ngInject */
    function ChooseTeamsCtrl($state, $scope, ContestAppService) {

        _getAllTeams();
        _getChosenTeams();
        
        $scope.chooseTeam = function(team, index) {
            ContestAppService.addTeamToContest(team);
            _getChosenTeams();
        };

        $scope.removeTeam = function(team, index) {
            ContestAppService.removeTeamFromContest(team);
            _getChosenTeams();
        };

        $scope.goAddPlayers = function() {
            $state.go('addPlayers');
        }

        // // // // // // // //
        // private functions //
        // // // // // // // //

        $scope._ifTeamChosen = function(team) {
            var isChosen = false;

            $scope.teamsChosen.forEach(function(teamChosen) {
                if(teamChosen.id === team.id) {
                   isChosen = true; 
                }
            });

            return isChosen;
        }

        function _getChosenTeams() {
            ContestAppService.getTeamsAsync('chosen')
                .then(function(teams) {
                    $scope.teamsChosen = teams;
                });
        }

        function _getAllTeams() {
            ContestAppService.getTeamsAsync('all')
                .then(function(teams) {
                    $scope.allTeams = teams;
                });
        }

    }
})();
