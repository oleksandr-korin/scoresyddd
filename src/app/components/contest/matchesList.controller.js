
(function() {
    'use strict';

    angular
        .module('scoresy')
        .controller('MatchesListCtrl', MatchesListCtrl);

    /** @ngInject */
    function MatchesListCtrl($state, $scope, ContestAppService, MatchAppService, $mdDialog, $mdSidenav, $rootScope, $timeout) {
        _getAllMatches();

        $scope.deleteMatch = function(match, index) {
            $mdDialog.show({
                controller: MatchesListCtrl,
                templateUrl: 'app/components/contest/templates/modal/deleteMatchDialog.html',
                parent: angular.element(document.body),
                // clickOutsideToClose: true
            }).then(function() {
                ContestAppService.removeMatchFromContest(match);
                _getAllMatches();
                $state.reload();
            });
        };

        $scope.editMatch = function(team, index) {};

        // // // // // // // // // // // 
        // New match dialog functions // 
        // // // // // // // // // // // 
        $scope.teams = ContestAppService.getTeamsOfContest();

        $scope.createNewMatchDialog = function() {
            $mdDialog.show({
                controller: MatchesListCtrl,
                templateUrl: 'app/components/contest/templates/modal/newMatchDialog.html',
                parent: angular.element(document.body),
                // clickOutsideToClose: true
            }).then(function() {
                $state.go('matchInProgress');
            }, function() {
                $state.reload();
            });
        };

        _initNewNextMatchForTeams();

        $scope.startMatch = function() {
            MatchAppService.startMatch($scope.newMatchData);
            $mdDialog.hide();
            $state.go('matchInProgress');
        };

        $scope.finnishContest = function() {
            $mdDialog.show({
                controller: MatchesListCtrl,
                templateUrl: 'app/components/contest/templates/modal/finnishContestDialog.html',
                parent: angular.element(document.body),
            }).then(function() {
                ContestAppService.finnishContestAsync();
            });
        };

        $scope.confirmDialog = function() {
            $mdDialog.hide();
        };

        $scope.cancelDialog = function(isSideToggle) {
            $mdDialog.cancel();

            if (isSideToggle) {
                $scope.toggleSideNav();
            }
            $state.reload();
        };

        $scope.toggleSideNav = function() {
            return $mdSidenav('sidenav')
                .toggle()
        };

        $rootScope.$on('toggleSidenav', function() {
            $scope.toggleSideNav();
        });

        // // // // // // // //
        // private functions //
        // // // // // // // //

        function _getAllMatches() {
            $scope.allMatches = ContestAppService.getCompletedMatches();
        }

        function _initNewNextMatchForTeams() {
            var nextTeamsIds, team1Index, team2Index;

            if (!$scope.allMatches.length ||
                 ($scope.allMatches.length === 1 &&
                  $scope.allMatches[0].matchResult === 'DRAW') ) return;

            $scope.newMatchData = {};

            nextTeamsIds = ContestAppService.getNextTeamsToPlayIds();

            angular.forEach($scope.teams, function(team, index) {
                if (nextTeamsIds.team_1Id === team.id) {
                    team1Index = index;
                }
            });

            angular.forEach($scope.teams, function(team, index) {
                if (nextTeamsIds.team_2Id === team.id) {
                    team2Index = index;
                }
            });

            $scope.newMatchData.team_1 = $scope.teams[team1Index];
            $scope.newMatchData.team_2 = $scope.teams[team2Index];
        }

    }
})();
