(function() {
    'use strict';

    angular
        .module('scoresy')
        .directive('navbar', navbar);

    /** @ngInject */
    function navbar() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/navbar/navbar.html',
            scope: {
                ifSearch: '=',
                ifSidenav: '=',
                navBack: '@',
                // ifNavBack: '=',
                search: '='
            },
            link: linkFunc,
            controller: NavbarController,
            controllerAs: 'navCtrl',
            bindToController: true
        };

        return directive;

        function linkFunc(scope, el, attr, navCtrl) {}

        /** @ngInject */
        function NavbarController($state, $scope, $rootScope) {
            var navCtrl = this;

            if ($state.current.data) {
                $scope.title = $state.current.data.title;
            }

            navCtrl.apply = function() {
                $rootScope.$broadcast('refreshPlayersList');
            }

            navCtrl.navigateBack = function() {
                $state.go(navCtrl.navBack);
            }

            navCtrl.clearSearch = function() {
                $rootScope.$broadcast('applySearch', {string: ''});
            };

            navCtrl.applySearch = function(string) {
                $rootScope.$broadcast('applySearch', {string: string});
            };

            navCtrl.toggleSidenav = function() {
                $rootScope.$broadcast('toggleSidenav');
            };
        }
    }

})();
