(function() {
    'use strict';

    angular
        .module('scoresy')
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider, $urlRouterProvider, $httpProvider, apiEndPointsConstant) {
        $stateProvider
            .state('login', {
                url: '/',
                data: {
                    title: "Scoresy"
                },
                templateUrl: 'app/login/login.html',
                controller: 'LoginController',
                controllerAs: 'login'
            })

            .state('contestList', {
                url: '/contestList',
                data: {
                    title: "Contest List"
                },
                templateUrl: 'app/components/contest/templates/contestList.html',
                controller: 'ContestCtrl',
                controllerAs: 'contest'
            })

            .state('createContest', {
                url: '/createContest',
                data: {
                    title: "Создать игровой день"
                    // title: "Create Contest"
                },
                templateUrl: 'app/components/contest/templates/contestCreate.html',
                controller: 'ContestCtrl',
                controllerAs: 'contest'
            })

            .state('chooseTeams', {
                url: '/choose-teams',
                data: {
                    title: "Выбор команд"
                    // title: "Choose Teams"
                },
                templateUrl: 'app/components/contest/templates/chooseTeams.html',
                controller: 'ChooseTeamsCtrl',
                controllerAs: 'chooseTeams'
            })

            .state('addPlayers', {
                url: '/add-players',
                data: {
                    title: "Добавление игроков"
                    // title: "Add Players"
                },
                templateUrl: 'app/components/contest/templates/addPlayers.html',
                controller: 'AddPlayersCtrl',
                controllerAs: 'addPlayers'
            })

            .state('matchesList', {
                url: '/matches-List',
                data: {
                    title: "Сыгранные матчи"
                    // title: "Matches List"
                },
                templateUrl: 'app/components/contest/templates/matchesList.html',
                controller: 'MatchesListCtrl',
                controllerAs: 'matchesList'
            })

            .state('matchInProgress', {
                url: '/match-in-progress',
                data: {
                    title: "Текущий матч"
                    // title: "Match In Progress"
                },
                templateUrl: 'app/components/contest/templates/matchInProgress.html',
                controller: 'MatchInProgressCtrl',
                controllerAs: 'matchInProgress'
            });

        $urlRouterProvider.otherwise('/');

        // custom headers set for Parse.com integration
        $httpProvider.defaults.headers.common['X-Parse-Application-Id'] = apiEndPointsConstant.appId;
        $httpProvider.defaults.headers.common['X-Parse-REST-API-Key'] = apiEndPointsConstant.restApiKey;
    }

})();
