(function() {
    'use strict';

	angular.module('scoresy')
		.factory('ValidateService', function() {

			/**
			 * Constructor, with class name
			 */
			function ValidateService() {}

			/**
		     * Validate match object of the parentObj
		     *
		     * @throw {Error} If value is not an instance of {MatchAggregate}
		     */
		    ValidateService.prototype.validateMatchAggregate = function(matchObj, property, parentObj) {
		        if (!matchObj instanceof parentObj.constructor) {
		            throw new Error('"' + property + '" object should be an instance of {MatchAggregate}',
		                parentObj.constructor.name);
		        }
		    };

		    /**
		     * Validate team object of the parentObj
		     *
		     * @throw {Error} If value is not an instance of {MatchAggregate}
		     */
		    ValidateService.prototype.validateTeamEntity = function(teamObj, property, parentObj) {
		        if (!teamObj instanceof parentObj.constructor) {
		            throw new Error('"' + property + '" object should be an instance of {TeamEntity}',
		                parentObj.constructor.name);
		        }
		    };

		    /**
		     * Validate PlayerEntity object of parentObj
		     *
		     * @throw {Error} If value is not an instance of {PlayerEntity}
		     */
		    ValidateService.prototype.validatePlayerEntity = function(playerObj, property, parentObj) {
		        if (!playerObj instanceof parentObj.constructor) {
		            throw new Error('"' + property + '" object should be an instance of {PlayerEntity}',
		                parentObj.constructor.name);
		        }
		    };

		    /**
		     * Validate activity object of parentObj
		     *
		     * @throw {Error} If value is not an instance of {ActivityEntity}
		     */
		    ValidateService.prototype.validateActivityEntity = function(activityObj, property, parentObj) {
		        if (!activityObj instanceof parentObj.constructor) {
		            throw new Error('"' + property + '" object should be an instance of {ActivityEntity}',
		                parentObj.constructor.name);
		        }
		    };

		    /**
		     * Validate date 
		     *
		     * @throw {Error} If value is not an instance of {Date}
		     */
		    ValidateService.prototype.validateDate = function(dateObj, property, parentObj) {
		        if (!dateObj instanceof Date) {
		            throw new Error('\'Date\' value should be an instance of {Date}',
		                parentObj.constructor.name);
		        }
		    };

		    ValidateService.prototype.validateString = function(value, property, parentObj) {
		        var type = typeof value;

		        if ('string' !== type) {
		            throw new Error('"' + property + '" value should be a {string}, {' + type + '} is given',
		                parentObj.constructor.name);
		        }
		    };

		    ValidateService.prototype.validateNumber = function(value, property, parentObj) {
		        var type = typeof value;

		        if ('number' !== type) {
		            throw new Error('"' + property + '" value should be a {number}, {' + type + '} is given',
		                parentObj.constructor.name);
		        }
		    };

			/**
			 * Return created singleton object
			 */
			return new ValidateService();
		});

    
})();


