(function() {
    'use strict';

	angular.module('scoresy')
		.factory('RemoteDataService', function($http, apiEndPointsConstant) {

			/**
			 * Constructor, with class name
			 */
			function RemoteDataService() {}

			// RemoteDataService.prototype.login = function(loginData) {
			// 	return $http({
			// 		method: 'POST',
			// 		url: apiEndPointsConstant.login,
			// 		data: loginData
			// 	});
			// };

			RemoteDataService.prototype.getDataListForClass = function(ClassName) {
				var method = 'get' + ClassName + 's' ;

				return $http({
					method: 'GET',
					url: apiEndPointsConstant[method]
				});
			};

			RemoteDataService.prototype.addNewPlayer = function(newPlayer) {
				var username = newPlayer.first + ' ' + newPlayer.last;

				return $http({
					method: 'POST',
					url: apiEndPointsConstant.addNewPlayer,
					data: {
						username: username
					}
				});
			};

			RemoteDataService.prototype.saveCompletedContest = function(contest) {
				return $http({
					method: 'POST',
					url: apiEndPointsConstant.saveContest,
					data: {
						contest: contest
					}
				});
			};

			/**
			 * Return created singleton object
			 */
			return new RemoteDataService();
		});

    
})();


