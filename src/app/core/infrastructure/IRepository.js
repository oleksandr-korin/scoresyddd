(function() {
	'use strict';

	angular
		.module('scoresy')
		.factory('IRepository', IRepository);

// @TODO review localStorage data persistance

	/** @ngInject */
	function IRepository(localStorageService, $http, ContestAggregateFactory, MatchAggregateFactory, $cookies) {
		var service;

		service = {
			saveContest: saveContest,
			saveMatchInProgress: saveMatchInProgress,
			getMatchInProgress: getMatchInProgress,
			getContest: getContest,
			saveUserProfile: saveUserProfile,
			getUserProfile: getUserProfile
		};

		return service;

		function saveUserProfile(userProfile) {
			var today = new Date(),
			expirationDate = today.getFullYear() + '/' + today.getMonth() + '/' + (today.getDate() + 1);
			// $cookies.put('userProfile', 'userProfile');			
			// $cookies.put('userProfile', userProfile, {expires: new Date(expirationDate)});			
		}

		function getUserProfile() {
			// return $cookies.getObject('userProfile');
		}

		function saveContest(contestObj) {
			localStorageService.set('ContestAggregate', contestObj);			
		}

		function getContest() {
			var contestObj, contestFromLocalStorage;
				contestFromLocalStorage = localStorageService.get('ContestAggregate');

				if (contestFromLocalStorage) {
					return ContestAggregateFactory.createFromObj(contestFromLocalStorage);
				}

				return ContestAggregateFactory.create();
		}

		function saveMatchInProgress(matchObj) {
			localStorageService.set('MatchInProgress', matchObj);			
		}

		function getMatchInProgress() {
			var matchObj, activeMatchFromLocalStorage;
				activeMatchFromLocalStorage = localStorageService.get('MatchInProgress');

				if (activeMatchFromLocalStorage) {
					return MatchAggregateFactory.createFromObj(activeMatchFromLocalStorage);
				}

				return MatchAggregateFactory.create();
		}
	}
})();
