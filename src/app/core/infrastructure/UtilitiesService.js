(function() {
    'use strict';

	angular.module('scoresy')
		.factory('UtilitiesService', function() {

			/**
			 * Constructor, with class name
			 */
			function UtilitiesService() {}

			/**
			 * random string used for object ids
			 */
			UtilitiesService.prototype.createId = function() {
				return Math.random().toString(36).substr(2, 5);
			};

			/**
		     * makes property of object unchangable, including change through descriptor
		     * if isConfigurable arguments is true, still can be changed through descriptor
		     */
			UtilitiesService.prototype.freezeProp = function(ownerObject, propName, isConfigurable) {
		    	var descriptor = Object.getOwnPropertyDescriptor(ownerObject, propName);
		    	descriptor.writable = false;
	  			descriptor.configurable = false;

	  			if (isConfigurable) descriptor.configurable = true;

	  			Object.defineProperty(ownerObject, propName, descriptor);
	  		}

	  		/**
		     * makes property of object writable, will not work if descriptor.configurable
		     * was set to false previuosly
		     */
		    UtilitiesService.prototype.unFreezeProp = function(ownerObject, propName) {
		    	var descriptor = Object.getOwnPropertyDescriptor(ownerObject, propName);
		    	descriptor.writable = true;
	  			Object.defineProperty(ownerObject, propName, descriptor);
		    }

			/**
			 * Return created singleton object
			 */

			return new UtilitiesService();
		});

})();