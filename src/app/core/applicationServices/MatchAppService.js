(function() {
	'use strict';

	angular
		.module('scoresy')
		.factory('MatchAppService', MatchAppService);

// @TODO review implementation of this kind of services!

	/** @ngInject */
	function MatchAppService(ContestAppService, MatchAggregateFactory, IRepository, ActivityEntityFactory, ActivityEnumerationValue) {
		var contest, thisService;
		
		thisService = {
			startMatch: startMatch,
			scoreTheGoal: scoreTheGoal,
			finnishMatch: finnishMatch,
			getMatchInProgress: getMatchInProgress,
		};

		return thisService;

		function getMatchInProgress() {
			return IRepository.getMatchInProgress();
		}

		function startMatch(newMatchData) {
			var match = MatchAggregateFactory.createFromObj(newMatchData);	

			IRepository.saveMatchInProgress(match);
		}

		function scoreTheGoal(match, team, activityData, isAutoGoal) {
			var goal, assistance,
			activityEnumeration = new ActivityEnumerationValue();
			
			goal = ActivityEntityFactory.createFromObj({
				type: isAutoGoal ? activityEnumeration.const('AUTOGOAL') : activityEnumeration.const('GOAL'),
				owner: activityData.goalOwner,
				ownerTeamId: team.id
			})

			if (activityData.assistanceOwner) {
				assistance = ActivityEntityFactory.createFromObj({
					type: activityEnumeration.const('ASSIST'),
					owner: activityData.assistanceOwner,
					ownerTeamId: team.id
				})
				match.addActivity(assistance);
			}

			match.addActivity(goal);
			IRepository.saveMatchInProgress(match);

			// if a team scored a number of goals that is equal to goals to win
			// finish match and return true to controller
			if (match.getTeam1Score() === match.goalsToWin || match.getTeam2Score() === match.goalsToWin ) {
				thisService.finnishMatch();
				return true;
			}
		}

		function deleteActivity() {

		}

		function finnishMatch() {
			var contest = ContestAppService.getContest(),
				match = thisService.getMatchInProgress(), 
				team1Id = match.team_1.id,
				team2Id = match.team_2.id,
				team1 = contest.getTeamById(team1Id),
				team2 = contest.getTeamById(team2Id);

				if (match.getTeam1Score() === match.getTeam2Score()) {
					team1.incrementPointsDraw();
					team2.incrementPointsDraw();
					match.setMatchResult = 'DRAW';
				}

				if (match.getTeam1Score() > match.getTeam2Score()) {
					team1.incrementPointsWin();
					match.setMatchResult = 'WINTEAM1';
				}

				if (match.getTeam2Score() > match.getTeam1Score()) {
					team2.incrementPointsWin();
					match.setMatchResult = 'WINTEAM2';
				}

				contest.updatePlayersGoalsScored(match.activities, team1Id, team2Id);

				contest.addMatch(match);

				IRepository.saveContest(contest);
		}

	}
})();
