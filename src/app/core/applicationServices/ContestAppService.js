(function() {
	'use strict';

	angular
		.module('scoresy')
		.factory('ContestAppService', ContestAppService); 

// @TODO review implementation of this kind of services!

	/** @ngInject */
	function ContestAppService($q, ContestAggregateFactory, MatchAggregateFactory, TeamEntityFactory, IRepository, RemoteDataService, PlayerEntityFactory, ActivityEnumerationValue, apiEndPointsConstant) {
		var thisService, activityEnumeration = new ActivityEnumerationValue();
		
		thisService = {
			// functions actions
			createNewContest: createNewContest,
			addTeamToContest: addTeamToContest,
			removeTeamFromContest: removeTeamFromContest,
			removeMatchFromContest: removeMatchFromContest,
			addNewPlayer: addNewPlayer,
			removePlayerFromTeam: removePlayerFromTeam,
			togglePlayerSelection: togglePlayerSelection,
			finnishContestAsync: finnishContestAsync,
			
			// functions getters
			getContest: getContest,
			getCompletedMatches: getCompletedMatches,
			getTeamsOfContest: getTeamsOfContest,
			getTeamsAsync: getTeamsAsync,
			getNextTeamsToPlayIds: getNextTeamsToPlayIds

		};

		return thisService;

		function createNewContest(contestData) {
			var contest = ContestAggregateFactory.createFromObj(contestData);
            	IRepository.saveContest(contest);		
			return contest;
		}

		function addTeamToContest(teamObj) {
			var contest;
			contest = thisService.getContest();
			contest.addTeam(teamObj);
			IRepository.saveContest(contest);
		}

		function removeTeamFromContest(teamObj) {
			var contest;

			contest = thisService.getContest();
			contest.removeTeam(teamObj);
			IRepository.saveContest(contest);
		}

		function getTeamsOfContest() {
			return thisService.getContest().teams;
		}

		function getNextTeamsToPlayIds() {
			var teams = angular.copy(thisService.getTeamsOfContest()),
				matches = thisService.getCompletedMatches(),
				lastMatch, beforeLastMatch, 
				team_1Id, team_2Id, team_2,
				teamsIdsObj = {};

				lastMatch = matches[matches.length - 1];
				beforeLastMatch = matches[matches.length - 2];

				team_2 = teams.filter(function(team) {
					return lastMatch.team_1.id !== team.id && lastMatch.team_2.id !== team.id
				});
				teamsIdsObj.team_2Id = team_2[0].id;

				if (lastMatch.getTeam1Score() > lastMatch.getTeam2Score()) {
					teamsIdsObj.team_1Id = lastMatch.team_1.id;
				}

				if (lastMatch.getTeam2Score() > lastMatch.getTeam1Score()) {
					teamsIdsObj.team_1Id = lastMatch.team_2.id;
				}

				if (lastMatch.getTeam2Score() === lastMatch.getTeam1Score()) {
					teamsIdsObj.team_1Id = _getNextTeam1IdWhenDraw();
				}

			return teamsIdsObj;


			function _getNextTeam1IdWhenDraw() {
				var team_1Id, teamPlayedPrevMatch;

				switch (lastMatch.team_1.id) {
					case beforeLastMatch.team_1.id:
					team_1Id = lastMatch.team_2.id;
					case beforeLastMatch.team_2.id:
					team_1Id = lastMatch.team_2.id;
				}

				switch (lastMatch.team_2.id) {
					case beforeLastMatch.team_1.id:
					team_1Id = lastMatch.team_1.id;
					case beforeLastMatch.team_2.id:
					team_1Id = lastMatch.team_1.id;
				}

				return team_1Id;
			}
		}

		function removeMatchFromContest(matchObj) {
			var contest;

			contest = thisService.getContest();
			contest.removeMatch(matchObj);
			IRepository.saveContest(contest);
		}

		function getContest() {
			var contest = IRepository.getContest();
			
			if (contest === null) {
				contest = ContestAggregateFactory.create();
			}

			return contest;
		}

		function getCompletedMatches() {
			return this.getContest().matches;
		}

		function getTeamsAsync(type) {
			var teams = [], teamEntity;

			if (type === 'all') {
				return RemoteDataService.getDataListForClass('Team').then(function(response) {
		            angular.forEach(response.data.results, function(teamObj) {
						teamEntity = TeamEntityFactory.createFromObj(teamObj);
		            	teams.push(teamEntity);
		            });

		            return teams;
		        });
			}

			if (type === 'chosen') {
				return $q.when(thisService.getTeamsOfContest());
			}
		}

		function finnishContestAsync() {
			var contest = thisService.getContest(),
				reportString = _createReportString(contest);

				return RemoteDataService.saveCompletedContest({contest: contest, report: reportString});
		}

		// @TODO implementation needs to be reviewed
		function _createReportString(contest) {
			var reportString = '', teams, matches, scoredPlayers;

			teams = contest.getTeams();
			matches = contest.getMatches();
			scoredPlayers = _getAllScoredPlayersOfContest(teams);

			teams.forEach(function(team) {
				var players = team.getPlayers();
				reportString += team.getName() + ': ';

				angular.forEach(players, function(player, index) {
					var closeString = ', ';
					if (players.length === index + 1) closeString = '. ';

					reportString += player.getName() + closeString;

				});
			});

			matches.forEach(function(match) {
				var activities = match.getActivities();
				reportString += match.getTeam1().getName() + ' ' + match.getTeam1Score() + ' : ' + match.getTeam2Score() + ' ' + match.getTeam2().getName();

				activities.forEach(function(activity, index) {
					var closeString = ', ';

					if (activityEnumeration.check('GOAL', activity.getType())) {
						if (activities.length === index + 1) closeString = ') | ';

						reportString += ' (' + activity.getOwner().getName() + closeString;
					}

				});
			});

			teams.forEach(function(team, index) {
				var closeString = ', ', openString = '';
				if (teams.length === index + 1) closeString = '. ';
				if (index === 0) openString = '///';


				reportString += openString + team.getName() + ' ' + team.getContestPoints() + closeString;
				
			});

			scoredPlayers.forEach(function(player, index) {
				var closeString = ', ', openString = '';
				if (scoredPlayers.length === index + 1) closeString = '. ';
				if (index === 0) openString = '///';

				reportString += openString + player.getName() + ' ' + player.getContestGoals() + closeString;
			});

			return reportString;
		}

		function _getAllScoredPlayersOfContest(teams) {
			var players = [];

			angular.forEach(teams, function(team) {
				angular.forEach(team.getPlayers(), function(player) {
					if (player.getContestGoals() > 0) {
						players.push(player);
					}
				});
			});

			return players;
		}

		function togglePlayerSelection(team, player) {
			var playerEntity, contest = this.getContest();
				playerEntity = PlayerEntityFactory.createFromObj(player);
				team.togglePlayerSelection(playerEntity);

				contest.removeTeam(team);
				contest.addTeam(team);

			IRepository.saveContest(contest);	
		}

		function removePlayerFromTeam(team, player) {
			var contest = this.getContest();
				team.removePlayer(player);

			IRepository.saveContest(this.getContest());	
		}

		function addNewPlayer(newPlayer) {
			return RemoteDataService.addNewPlayer(newPlayer);
		}
	}
})();
