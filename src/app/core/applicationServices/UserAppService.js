(function() {
	'use strict';

	angular
		.module('scoresy')
		.factory('UserAppService', UserAppService);

// @TODO review implementation of this kind of services!

	/** @ngInject */
	function UserAppService(IRepository, $cookies, $http, apiEndPointsConstant) {
		var contest, thisService;
		
		thisService = {
			login: login,
			isLoggedIn: isLoggedIn
		};

		return thisService;

		function login(loginData) {
			return $http({
				method: 'POST',
				url: apiEndPointsConstant.login,
				data: loginData
			}).then(function(response) {
				var user = response.data.result;

				user.isLoggedIn = true;
				IRepository.saveUserProfile(user);

				return user;
			});

		}

		function isLoggedIn(newMatchData) {
			var isLoggedIn = true, userProfile;
				userProfile = IRepository.getUserProfile();
				
			return isLoggedIn;
		}

	}
})();
