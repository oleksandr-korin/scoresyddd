(function() {
    'use strict';

	angular.module('scoresy')
		.factory('PlayerEntityFactory', function(PlayerEntity) {

			/**
			 * Constructor, with class name
			 */
			function PlayerEntityFactory() {}

			PlayerEntityFactory.prototype.createFromObj = function(playerDataObj) {
				if (!playerDataObj) return;
				
				var player = new PlayerEntity();

				player.setId = playerDataObj.objectId || playerDataObj.id;
				player.setName = playerDataObj.username || playerDataObj.name;
				player.setContestScoredGoals = playerDataObj.contestScoredGoals || 0;

				return player;
			};

			/**
			 * Return created singleton object
			 */
			return new PlayerEntityFactory();
		});

    
})();


