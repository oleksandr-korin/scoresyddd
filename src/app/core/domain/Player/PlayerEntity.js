(function() {
    'use strict';

	angular.module('scoresy')
		.factory('PlayerEntity', function(UtilitiesService, ValidateService) {

			/**
			 * Constructor, with class name
			 */
			function PlayerEntity() {
				/**
	             * id
	             *
	             * @type string
	             */
				this.id = '';

				/**
	             * Player name
	             *
	             * @type string
	             */
				this.name = '';
				
				/**
	             * Number of goals scored in current contest
	             *
	             * @type number
	             */
				this.contestScoredGoals = 0;
				
				// setters
				Object.defineProperties(this, {
					
					/**
		             * set player id method
		             */
					setId: {
						set: function(value) {
					  		ValidateService.validateString(value, 'id', this);
					  		this.id = value;

				  			UtilitiesService.freezeProp(this, 'id');		
					    }
					},

					/**
		             * set player name method
		             */
					setName: {
						set: function(value) {
					  		ValidateService.validateString(value, 'name', this);
					  		this.name = value;

				  			UtilitiesService.freezeProp(this, 'name');
					    }
					},

					/**
		             * set contest scored goals method
		             */
					setContestScoredGoals: {
					  	set: function(value) {
					  		ValidateService.validateNumber(value, 'contestScoredGoals', this);
					  		this.contestScoredGoals = value;

				  			UtilitiesService.freezeProp(this, 'contestScoredGoals', true);		
					    }
					},

					/**
				     * increase number of goals in current contest
				     */
					doIncreaseContestGoals: {
					  	get: function() {
					  		var descriptor = Object.getOwnPropertyDescriptor(this, 'contestScoredGoals'),
					  			propName = 'contestScoredGoals';

					  		UtilitiesService.unFreezeProp(this, propName);

					  		this.contestScoredGoals++;

				  			UtilitiesService.freezeProp(this, propName, true);		
					    }
					},

					/**
				     * decrease number of goals in current contest
				     */
					doDecreaseContestGoals: {
					  	get: function() {
					  		var descriptor = Object.getOwnPropertyDescriptor(this, 'contestScoredGoals'),
					  			propName = 'contestScoredGoals';

					  		UtilitiesService.unFreezeProp(this, propName);

					  		this.contestScoredGoals--;

				  			UtilitiesService.freezeProp(this, propName, true);		
					    }
					}

				});
			}

			return PlayerEntity;
		});

})();
