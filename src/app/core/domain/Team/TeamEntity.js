(function() {
    'use strict';

	angular.module('scoresy')
		.factory('TeamEntity', function(UtilitiesService, ValidateService) {

			/**
			 * Constructor, with class name
			 */
			function TeamEntity() {
				/**
	             * id
	             *
	             * @type string
	             */
				this.id = '';
				/**
	             * team name
	             *
	             * @type string
	             */
				this.name = '';
				/**
	             * color
	             * Reperesents a color in '#000000' format
	             *
	             * @type string
	             */
				this.color = '';
				/**
	             * players
	             *
	             * @type an array of PlayerEntity
	             */
				this.players = [];
				/**
	             * contest points
	             *
	             * @type number
	             */
				this.contestPoints = 0;

				// getters and setters
				Object.defineProperties(this, {

					/**
		             * set id method
		             */
					setId: {
					  	set: function(value) {
					  		var fieldName = 'id';
					  		ValidateService.validateString(value, fieldName, this);
					  		this.id = value;

				  			UtilitiesService.freezeProp(this, fieldName);		
					    }
					},

					/**
		             * set name method
		             */
					setName: {
					  	set: function(value) {
					  		var fieldName = 'name';
					  		ValidateService.validateString(value, fieldName, this);
					  		this.name = value;

				  			UtilitiesService.freezeProp(this, fieldName);		
					    }
					},

					/**
		             * set color method
		             */
					setColor: {
					  	set: function(value) {
					  		var fieldName = 'color';
					  		ValidateService.validateString(value, fieldName, this);
					  		this[fieldName] = value;

				  			UtilitiesService.freezeProp(this, fieldName);		
					    }
					},

					/**
		             * set points of contest
		             */
					setContestPoints: {
					  	set: function(value) {
					  		var fieldName = 'contestPoints';
					  		ValidateService.validateNumber(value, fieldName, this);
					  		this[fieldName] = value;
					    }
					}
				});

			}
		    
			/**
		     * Add a player to the Team object
		     *
		     * @syntax addPlayer(playerObj)
		     * @param {object} PlayerEntity
		     * @returns {this.players}
		     */
			TeamEntity.prototype.addPlayer = function(playerObj) {
				ValidateService.validatePlayerEntity(playerObj, 'player', this);
				this.players.push(playerObj);
			};

			/**
		     * Toggle player (add/remove) to the Team object
		     *
		     * @syntax togglePlayerSelection(playerObj)
		     * @param {object} PlayerEntity
		     * @returns {undefined}
		     */
			TeamEntity.prototype.togglePlayerSelection = function(playerObj) {
				ValidateService.validatePlayerEntity(playerObj, 'player', this);

				if (this.checkIfAdded(playerObj)) {
					this.removePlayer(playerObj);
				} else {
					this.players.push(playerObj);
				}
			};

			/**
		     * Remove player from the Team object
		     *
		     * @syntax removePlayer(playerObj)
		     * @param {object} PlayerEntity
		     * @returns {undefined}
		     */
			TeamEntity.prototype.removePlayer = function(playerObj) {
				var players, indexToRemove;
					players = this.players;

					players.forEach(function(player, index) {
						if (player.id === playerObj.id) {
							indexToRemove = index;
						}
					});

				this.players.splice(indexToRemove, 1);
			};

			/**
		     * Check if player is added to the Team object
		     *
		     * @syntax checkIfAdded(playerObj)
		     * @param {object} PlayerEntity
		     * @returns {boolean}
		     */
			TeamEntity.prototype.checkIfAdded = function(playerToCheck) {
				var players, isAdded = false;
					players = this.players;

					players.forEach(function(player) {
						if (player.id === playerToCheck.objectId) {
							isAdded = true;
							return;
						}
						if (playerToCheck.id && player.id === playerToCheck.id) {
							isAdded = true;
							return;
						}
					});

					return isAdded;
			};

			/**
		     * Get number of players in the Team object
		     *
		     * @syntax getNumberOfAddedPlayers()
		     * @returns {number} this.players.length
		     */
			TeamEntity.prototype.getNumberOfAddedPlayers = function() {
				return this.players.length;
			};

			/**
		     * Increment when draw
		     *
		     * @syntax incrementPointsDraw()
		     * @returns {undefined}
		     */
			TeamEntity.prototype.incrementPointsDraw = function() {
				this.contestPoints += 1;
			};

			/**
		     * Decrement when draw
		     *
		     * @syntax decrementPointsDraw()
		     * @returns {undefined}
		     */
			TeamEntity.prototype.decrementPointsDraw = function() {
				this.contestPoints -= 1;
			};

			/**
		     * Increment when win (used when delete a match from contest)
		     *
		     * @syntax incrementPointsWin()
		     * @returns {undefined}
		     */
			TeamEntity.prototype.incrementPointsWin = function() {
				this.contestPoints += 3;
			};

			/**
		     * Decrement when win (used when delete a match from contest)
		     *
		     * @syntax decrementPointsWin()
		     * @returns {undefined}
		     */
			TeamEntity.prototype.decrementPointsWin = function() {
				this.contestPoints -= 3;
			};

			/**
			 * Return the constructor function
			 */
			return TeamEntity;
		});

})();


