(function() {
    'use strict';

	angular.module('scoresy')
		.factory('TeamEntityFactory', function(TeamEntity, PlayerEntityFactory, UtilitiesService) {

			/**
			 * Constructor, with class name
			 */
			function TeamEntityFactory() {}

			TeamEntityFactory.prototype.createFromObj = function(teamDataObj) {
				var team = new TeamEntity(), newPlayer;
				teamDataObj.players = teamDataObj.players || [];

				team.setId = teamDataObj.objectId || teamDataObj.id || UtilitiesService.createId();
				team.setName = teamDataObj.name;
				team.setColor = teamDataObj.color;
				team.setContestPoints = teamDataObj.contestPoints || 0;

				teamDataObj.players.forEach(function(player) {
					newPlayer = PlayerEntityFactory.createFromObj(player);

					team.addPlayer(newPlayer);
				});

				return team;
			};

			/**
			 * Return created singleton object
			 */
			return new TeamEntityFactory();
		});

    
})();


