(function() {
    'use strict';

	angular.module('scoresy')
		.factory('ContestAggregateFactory', function(ContestAggregate, MatchAggregateFactory, TeamEntityFactory) {

			/**
			 * Constructor, with class name
			 */
			function ContestAggregateFactory() {}

			ContestAggregateFactory.prototype.create = function() {
				var contest = new ContestAggregate();

				return contest;
			};

			ContestAggregateFactory.prototype.createFromObj = function(contestObject) {
				var contest = new ContestAggregate();
				contestObject.matches = contestObject.matches || [];
				contestObject.teams = contestObject.teams || [];

					contest.setId = contestObject.id || '';
					contest.setDate = contestObject.date || new Date();
					contest.setTitle = contestObject.title || '';

					contestObject.matches.forEach(function(matchObj) {
						var completeMatch = MatchAggregateFactory.createFromObj(matchObj);

						contest.addMatch(completeMatch);
					});

					contestObject.teams.forEach(function(teamObj) {
						var team = TeamEntityFactory.createFromObj(teamObj);

						contest.addTeam(team);
					});

				return contest;
			};

			/**
			 * Return created singleton object
			 */
			return new ContestAggregateFactory();
		});

    
})();


