(function() {
    'use strict';

	angular.module('scoresy')
		.factory('ContestAggregate', function(ActivityEnumerationValue, UtilitiesService, ValidateService) {

			/**
			 * Constructor, with class name
			 */
			function ContestAggregate() {
				/**
	             * id
	             *
	             * @type string
	             */
				this.id = '';
				/**
	             * Date object
	             * Reperesents Date of the Contest to take place
	             *
	             * @type object (new Date())
	             */
				this.date = new Date();
				/**
	             * Title
	             * Title of the Contest
	             *
	             * @type string
	             */
				this.title = '';
				/**
	             * teams
	             * Teams to participate
	             *
	             * @type array (Team)
	             */
				this.teams = [];
				/**
	             * Completed Matches
	             * Reperesents matches that are completed
	             *
	             * @type array (Match Entity)
	             */
				this.matches = [];


				// getters and setters
				Object.defineProperties(this, {

					/**
		             * set id method
		             */
					setId: {
					  	set: function(value) {
					  		var fieldName = 'id';
					  		ValidateService.validateString(value, fieldName, this);
					  		this.id = value;

				  			UtilitiesService.freezeProp(this, fieldName);		
					    }
					},

					/**
		             * set date method
		             */
					setDate: {
					  	set: function(value) {
					  		ValidateService.validateDate(value, this);
					  		this.date = value;

				  			UtilitiesService.freezeProp(this, 'date');		
					    }
					},

					/**
		             * set title method
		             */
					setTitle: {
					  	set: function(value) {
					  		var fieldName = 'title';
					  		ValidateService.validateString(value, fieldName, this);
					  		this.title = value;

				  			UtilitiesService.freezeProp(this, fieldName);		
					    }
					},

					/**
		             * get number of matches method
		             */
					numberOfMatches: {
					  	get: function(value) {
					  		return this.matches.length;
					    }
					}
				});

			}

			ContestAggregate.prototype.addMatch = function(completedMatchObj) {
				ValidateService.validateMatchAggregate(completedMatchObj, 'match', this);
				this.matches.push(completedMatchObj);
			};

			ContestAggregate.prototype.removeMatch = function(completedMatchObj) {
				var matches, indexToRemove;
					matches = this.matches, self = this;

					matches.forEach(function(match, index) {
						if (match.id === completedMatchObj.id) {
							indexToRemove = index;

							_recalculateTeamPoints(match);

							matches.splice(indexToRemove, 1);
						}
					});

					function _recalculateTeamPoints(match) {
						var teams = self.teams,
						team1Id = match.team_1.id,
						team2Id = match.team_2.id,
						team1 = self.getTeamById(team1Id),
						team2 = self.getTeamById(team2Id),
						activities = match.activities;

						switch (match.matchResult) {
							case 'DRAW':
								team1.decrementPointsDraw();
								team2.decrementPointsDraw();
							break;
							case 'WINTEAM1': 
								team1.decrementPointsWin();
								_recalculateGoalsScoredForPlayers(team1.players, activities);
							break;
							case 'WINTEAM2': 
								team2.decrementPointsWin();
								_recalculateGoalsScoredForPlayers(team2.players, activities);
							break;
						}
					}

					function _recalculateGoalsScoredForPlayers(players, activities) {
						activities.forEach(function(activity) {
							var goalOwner = activity.owner;

							players.forEach(function(player) {
								if (goalOwner.id === player.id) {
									player.doDecreaseContestGoals;
								}
							});
						});
					}
			};

			ContestAggregate.prototype.getTeamById = function(teamId) {
				return this.teams.find(function(team) {
					return team.id === teamId;
				});
			};

			ContestAggregate.prototype.addTeam = function(teamObj) {
				ValidateService.validateTeamEntity(teamObj, 'team', this);
				this.teams.push(teamObj);

				if (this.teams.length === 3) {
					UtilitiesService.freezeProp( this, 'teams');
				}

			};

			ContestAggregate.prototype.removeTeam = function(teamObj) {
				var indexToRemove, teams = this.teams;

					teams.forEach(function(team, index) {
						if (team.id === teamObj.id) {
							indexToRemove = index;

							teams.splice(indexToRemove, 1);
						}
					});
			};

		    ContestAggregate.prototype.updatePlayersGoalsScored = function(activities, team1Id, team2Id) {
		    	var team1, team2, activityEnumeration = new ActivityEnumerationValue();
		    		team1 = this.getTeamById(team1Id);
		    		team2 = this.getTeamById(team2Id);

		    		activities.forEach(function(activity) {
		    			if(activityEnumeration.check('GOAL', activity.type)) {

		    				team1.players.forEach(function(player) {
		    					if (player.id === activity.owner.id) {
		    						player.doIncreaseContestGoals;
		    					}
		    				});

		    				team2.players.forEach(function(player) {
		    					if (player.id === activity.owner.id) {
		    						player.doIncreaseContestGoals;
		    					}
		    				});

		    			}
		    		});
			};

			return ContestAggregate;
		});

    
})();


