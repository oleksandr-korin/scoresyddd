(function() {
    'use strict';

	angular.module('scoresy')
		.factory('MatchAggregate', function(FullTimeValue, TeamEntity, ActivityEntity, ActivityEnumerationValue, UtilitiesService, ValidateService) {
			var activityEnumeration = new ActivityEnumerationValue();

			/**	
			 * Constructor, with class name
			 */
			function MatchAggregate() {
				/**
	             * id
	             *
	             * @type string
	             */
				this.id = '';
				/**
	             * team 1 object
	             * Reperesents one of 2 competitor teams
	             *
	             * @type object (TeamEntity)
	             */
				this.team_1 = new TeamEntity();
				/**
	             * team 2 object
	             * Reperesents one of 2 competitor teams
	             *
	             * @type object (TeamEntity)
	             */
				this.team_2 = new TeamEntity();
				/**
	             * activities
	             * Activities of Active Match
	             *
	             * @type array (ActivityEntity)
	             */
				this.activities = [];
				/**
	             * Full Time object
	             * Reperesents a full time to play and options
	             *
	             * @type object (FullTime)
	             */
	             // @Not needed for now
				// this.fullTime = new FullTimeValue();
				/**
	             * Goals to win
	             *
	             * @type number
	             */
				this.goalsToWin = 0;
				/**
	             * Match result
	             *
	             * @type string 'DRAW', 'WINTEAM1', 'WINTEAM2'
	             */
				this.matchResult = '';


				// setters
				Object.defineProperties(this, {
					
					/**
		             * set player id method
		             */
					setId: {
						set: function(value) {
					  		ValidateService.validateString(value, 'id', this);
					  		this.id = value;

				  			UtilitiesService.freezeProp(this, 'id');		
					    }
					},

					/**
		             * set team 1 method
		             */
					setTeam1: {
						set: function(value) {
					  		ValidateService.validateTeamEntity(value, 'team_1', this);
					  		this.team_1 = value;

				  			UtilitiesService.freezeProp(this, 'team_1');
					    }
					},

					/**
		             * set team 2 method
		             */
					setTeam2: {
						set: function(value) {
					  		ValidateService.validateTeamEntity(value, 'team_2', this);
					  		this.team_2 = value;

				  			UtilitiesService.freezeProp(this, 'team_2');
					    }
					},

					/**
		             * set match result method
		             */
					setMatchResult: {
						set: function(value) {
					  		ValidateService.validateString(value, 'matchResult', this);
					  		this.matchResult = value;
					    }
					},

					/**
		             * set goals to win method
		             */
					setGoalsToWin: {
					  	set: function(value) {
					  		ValidateService.validateNumber(value, 'goalsToWin', this);
					  		this.goalsToWin = value;

				  			UtilitiesService.freezeProp(this, 'goalsToWin');		
					    }
					}

				});

			}

			MatchAggregate.prototype.addActivity = function(activityObj) {
				ValidateService.validateActivityEntity(activityObj, 'activity', this);
				this.activities.push(activityObj);
			};

		    /**
		     * Get team 1 score
		     *
		     */
		    MatchAggregate.prototype.getTeam1Score = function() {
				return _getTeamScore(this.team_1.id, this.team_2.id, this);
			}

			/**
		     * Get team 2 score
		     *
		     */
		    MatchAggregate.prototype.getTeam2Score = function() {
				return _getTeamScore(this.team_2.id, this.team_1.id, this);
			}

			/**
		     * Get team 1 goals owners
		     *
		     */
		    MatchAggregate.prototype.getTeam1GoalsOwners = function() {
				return _getTeamGoalsOwners(this.team_1.id, this.team_2.id, this);
			}

			/**
		     * Get team 2 goals owners
		     *
		     */
		    MatchAggregate.prototype.getTeam2GoalsOwners = function() {
				return _getTeamGoalsOwners(this.team_2.id, this.team_1.id, this);
			}

			/**
			 * private functions
			 */
			function _getTeamGoalsOwners(teamId, opponentTeamId, self) {
				var goalOwners = [];
					self.activities.forEach(function(activity) {
						if ( activity.ownerTeamId === teamId &&
							 activityEnumeration.check('GOAL', activity.type) ) {

							goalOwners.push(activity.owner);
						}

						if ( activity.ownerTeamId === opponentTeamId &&
							 activityEnumeration.check('AUTOGOAL', activity.type) ) {

							goalOwners.push(activity.owner);
						}
					});
				return goalOwners;
			}

			function _getTeamScore(teamId, opponentTeamId, self) {
				var score = 0;

					self.activities.forEach(function(activity) {
						if ( activity.ownerTeamId === teamId &&
							 activityEnumeration.check('GOAL', activity.type) ) {

							score++;
						}

						if ( activity.ownerTeamId === opponentTeamId &&
							 activityEnumeration.check('AUTOGOAL', activity.type) ) {

							score++;
						}

					});
				return score;
			}


			/**
			 * Return the constructor function
			 */
			return MatchAggregate;
		});
    
})();


