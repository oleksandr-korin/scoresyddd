(function() {
    'use strict';

	angular.module('scoresy')
		.factory('ActivityEntityFactory', function(ActivityEntity, ActivityEnumerationValue, PlayerEntityFactory, UtilitiesService) {

			/**
			 * Constructor, with class name
			 */
			function ActivityEntityFactory() {}

			/**
			 * Create new Activity Entity Object
			 */
			ActivityEntityFactory.prototype.create = function() {
				return new ActivityEntity();
			};

			/**
			 * Create new Activity Entity Object using existing data obj
			 */
			ActivityEntityFactory.prototype.createFromObj = function(newActivityData) {
				var activity = new ActivityEntity(), activityEnumeration;
					
				activityEnumeration = new ActivityEnumerationValue();

				activity.setId = newActivityData.id || UtilitiesService.createId();
				activity.setType = newActivityData.type;
				activity.setOwner = PlayerEntityFactory.createFromObj(newActivityData.owner);

				activity.setOwnerTeamId = newActivityData.ownerTeamId;

				return activity;
			};

			/**
			 * Return created singleton object
			 */
			return new ActivityEntityFactory();
		});
    
})();


