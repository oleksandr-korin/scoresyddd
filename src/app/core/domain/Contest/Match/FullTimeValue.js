(function() {
    'use strict';

	angular.module('scoresy')
		.factory('FullTimeValue', function() {

			/**
			 * Constructor, with class name
			 */
			function FullTimeValue(minutes, isHalfTime) {
				return {
					minutes: minutes,
					isHalfTime: isHalfTime
				};
			}

			/**
			 * Return the constructor function
			 */
			return FullTimeValue;
		});

    
})();


