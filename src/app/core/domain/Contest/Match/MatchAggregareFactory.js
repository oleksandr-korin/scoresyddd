(function() {
    'use strict';

	angular.module('scoresy')
		.factory('MatchAggregateFactory', function(MatchAggregate, TeamEntityFactory, ActivityEntityFactory, FullTimeValue, PlayerEntityFactory, UtilitiesService) {

			/**
			 * Constructor, with class name
			 */
			function MatchAggregateFactory() {}

			/**
			 * Create new Match Aggregate Object
			 */
			 // @TODO check if create factory method is usefull
			MatchAggregateFactory.prototype.create = function() {
				var match = new MatchAggregate();

				return match;
			};

			/**
			 * Create new Match Aggregate Object using existing data obj
			 */
			MatchAggregateFactory.prototype.createFromObj = function(newMatchData) {
				var match = new MatchAggregate(), team_1, team_2;
				newMatchData.activities = newMatchData.activities || [];
				
				match.setId = newMatchData.id || UtilitiesService.createId();
				match.setTeam1 = newMatchData.team_1;
				match.setTeam2 = newMatchData.team_2;
				match.setGoalsToWin = newMatchData.goalsToWin || 0;
				match.setMatchResult = newMatchData.matchResult || '';


				angular.forEach(newMatchData.activities, function(activityObj) {
					var activity = new ActivityEntityFactory.createFromObj(activityObj);

					match.addActivity(activity);
				});

				function _createTeam(teamObj) {
					return TeamEntityFactory.createFromObj(teamObj);
				}
				
				return match;
			};
			

			/**
			 * Return created singleton object
			 */
			return new MatchAggregateFactory();
		});

    
})();


