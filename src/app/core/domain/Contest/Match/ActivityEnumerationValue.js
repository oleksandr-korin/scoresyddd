(function() {
    'use strict';

    angular.module('scoresy')
        .factory('ActivityEnumerationValue', function(PlayerEntity, TeamEntity) {

            /**
             * a constant for Activity type
             */
            function ActivityEnumerationValue() {

                Object.defineProperties(this, {
                    GOAL: {
                        value: 1,
                        configurable: false,
                        writable: false,
                        enumerable: true
                    },
                    AUTOGOAL: {
                        value: 2,
                        configurable: false,
                        writable: false,
                        enumerable: true
                    },
                    ASSIST: {
                        value: 3,
                        configurable: false,
                        writable: false,
                        enumerable: true
                    },
                });

            }

            /**
             * Get constant value by name
             *
             * @param {string} constName
             * @returns {number}
             */
            ActivityEnumerationValue.prototype.const = function(constName) {
                return this[constName];
            };

            /**
             * Validate constant by name
             *
             * @param {string} constName
             * @returns {boolean}
             */
            ActivityEnumerationValue.prototype.validateName = function(constName) {
                if (undefined === this[constName]) {
                    return false;
                }

                throw new Error('Invalid type for Activity is provided ', 'ActivityEnumerationValue');
            };

            /**
             * Validate constant value by value
             *
             * @param {number} constValue
             * @returns {boolean}
             */
            ActivityEnumerationValue.prototype.validateValue = function(constValue) {
                var constName;
                for (constName in this) {
                    if (constValue === this[constName]) {
                        return true;
                    }
                }

                throw new Error('Invalid type for Activity is provided ', 'ActivityEnumerationValue');
            };

            /**
             * Check if value of a constant defined by name is equal to provided value
             *
             * @param {string} constName
             * @param {number} constValue
             * @returns {boolean}
             */
            ActivityEnumerationValue.prototype.check = function(constName, constValue) {
                if (constValue === this[constName]) {
                    return true;
                }

                return false;
            };

            /**
             * Return the constructor function
             */
            return ActivityEnumerationValue;
        });
    
})();


