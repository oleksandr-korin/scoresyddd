(function() {
    'use strict';

	angular.module('scoresy')
		.factory('ActivityEntity', function(ActivityEnumerationValue, PlayerEntity, UtilitiesService, ValidateService) {
			var activityEnumeration = new ActivityEnumerationValue();
			/**
			 * Constructor, with class name
			 */
			function ActivityEntity() {
					/**
		             * id
		             *
		             * @type string
		             */
					this.id = '';

					/**
		             * goal, violation, good defence etc. For now only 'GOAL' and 'ASSIST' implemented
		             *
		             * @type number, default 1
		             */
					this.type = activityEnumeration.const('GOAL');

					/**
		             *  the one who initiates activity 
					 *	e.g. scores a goal, gives a pass to goal, good defence action, commits a violation etc.
		             *
		             * @type PlayerEntity
		             */
					this.owner = new PlayerEntity();

					/**
		             *  team id that owner belongs to 
		             *
		             * @type string
		             */
					this.ownerTeamId = '';

					/**
		             * the one who took part in activity (the opposite of owner)
					 * e.g. misses a goal, gets a pass to goal etc. Not implemented untill a need
		             *
		             * @type PlayerEntity
		             */
					this.user = new PlayerEntity();

					// setters
					Object.defineProperties(this, {

						/**
			             * set id method
			             */
						setId: {
						  	set: function(value) {
						  		ValidateService.validateString(value, 'id', this);
						  		this.id = value;

					  			UtilitiesService.freezeProp(this, 'id');		
						    }
						},

						/**
			             * set type method
			             */
						setType: {
						  	set: function(value) {
						  		activityEnumeration.validateValue(value)
						  		this.type = value;

					  			UtilitiesService.freezeProp(this, 'type');		
						    }
						},

						/**
			             * set owner method
			             */
						setOwner: {
						  	set: function(valueObj) {
						  		ValidateService.validatePlayerEntity(valueObj, 'owner', this);
						  		this.owner = valueObj;

					  			UtilitiesService.freezeProp(this, 'owner');		
						    }
						},

						/**
			             * set owner team method
			             */
						setOwnerTeamId: {
						  	set: function(value) {
						  		ValidateService.validateString(value, 'ownerTeamId', this);
						  		this.ownerTeamId = value;

					  			UtilitiesService.freezeProp(this, 'ownerTeamId');
						    }
						},

						/**
			             * set user method
			             */
						setUser: {
						  	set: function(valueObj) {
						  		ValidateService.validatePlayerEntity(valueObj, 'user', this);
						  		this.user = valueObj;

					  			UtilitiesService.freezeProp(this, 'user');
						    }
						},
					});

			}

			return ActivityEntity;
		});

})();
